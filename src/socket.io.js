import io from "socket.io-client";

const socket = io.connect('https://api.devilez.com/', {
    path: '/musicpicker/socket.io/',
    transports: ["websocket"]
});

export default socket;
